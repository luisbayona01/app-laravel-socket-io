<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


#rutas  de cuentas
Route::get('/cuenta', function () {
    return view('cuenta.index');
});

 
Route::get('/liscuentas', [App\Http\Controllers\CuentaController::class, 'index'])->name('liscuentas');
Route::post('/createcuentas', [App\Http\Controllers\CuentaController::class, 'store'])->name('createcuentas');
Route::post('/editcuentas', [App\Http\Controllers\CuentaController::class, 'edit'])->name('editcuentas');
Route::post('/updatecuentas', [App\Http\Controllers\CuentaController::class, 'update'])->name('updatecuentas');
Route::post('/deletecuentas', [App\Http\Controllers\CuentaController::class, 'destroy'])->name('deletecuentas');


#rutas  pedidos
Route::get('/', function () {
    return view('pedido.index');
});

Route::get('/listpedidos', [App\Http\Controllers\PedidoController::class, 'index'])->name('listpedidos');
Route::get('/createpedidos',[App\Http\Controllers\PedidoController::class, 'create'])->name('createpedidos');
Route::post('/storepedidos',[App\Http\Controllers\PedidoController::class, 'store'])->name('store');
Route::post('/editpedidos',[App\Http\Controllers\PedidoController::class, 'edit'])->name('editpedidos');
Route::post('/updatepedidos',[App\Http\Controllers\PedidoController::class, 'update'])->name('updatepedidos');
Route::post('/deletepedidos', [App\Http\Controllers\PedidoController::class, 'destroy'])->name('deletepedidos');