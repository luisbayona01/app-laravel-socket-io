<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'pedido';

    /**
     * Run the migrations.
     * @table pedido
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idpedido');
            $table->integer('idcuenta')->nullable()->default(null)->unsigned();
            $table->string('producto', 45)->nullable()->default(null);
            $table->string('cantidad', 45)->nullable()->default(null);
            $table->string('valortotal', 45)->nullable()->default(null);

            $table->index(["idcuenta"], 'id_cuenta_idx');


            $table->foreign('idcuenta', 'id_cuenta_idx')
                ->references('idcuenta')->on('cuenta')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
