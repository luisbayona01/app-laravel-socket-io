<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use Illuminate\Http\Request;
use App\Models\Cuenta;
/**
 * Class PedidoController
 * @package App\Http\Controllers
 */
class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     //* @return \Illuminate\Http\Response
     */
    public function index()
    {   /*$categoriaproductos= Categoriaproducto::join('tiendas','tiendas.idtiendas','=','categoriaproductos.idtiendacategoriap')
        ->where('categoriaproductos.idtiendacategoriap', '=',session('iduseradmintienda'))
        ->get(['categoriaproductos.*', 'tiendas.nombre as tiendas']);*/

        $pedidos = Pedido::join('cuenta','cuenta.idcuenta','=','pedido.idcuenta')
         ->get(['pedido.*', 'cuenta.nombre as cuenta']);      
         return $pedidos;
        
    }

    /**
     * Show the form for creating a new resource.
     *
     //* @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pedido = new Pedido();
        $cuenta= Cuenta::pluck('nombre','idcuenta');
        return view('pedido.create', compact('pedido','cuenta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
          */
    public function store(Request $request)
    {
        request()->validate(Pedido::$rules);

        $pedido = Pedido::create($request->all());
if($pedido){
     $pedidoscuenta = Pedido::join('cuenta','cuenta.idcuenta','=','pedido.idcuenta')
 ->where('pedido.idpedido', '=',$pedido->idpedido)
         ->get(['pedido.*', 'cuenta.nombre as cuenta','cuenta.email','cuenta.telefono']);   

          return response()->json([
                'ok'    => true,
                'menssage'  => 'pedido creado exitosamente',
                'data'=> $pedidoscuenta 
                ]);

}

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
   /* public function show($id)
    {
        $pedido = Pedido::find($id);

        return view('pedido.show', compact('pedido'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     
     */
    public function edit(Request $request)
    { $id=$request->id;
        $pedido = Pedido::find($id);
       $cuenta= Cuenta::pluck('nombre','idcuenta');
        return view('pedido.edit', compact('pedido','cuenta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Pedido $pedido
    
     */
    public function update(Request $request)
    {



        request()->validate(Pedido::$rules);
         
$pedidoupdate=pedido::where('idpedido',$request->idpedido)->update([
            		'idcuenta' => $request->idcuenta,
     'producto'=> $request->producto,
     'cantidad'=> $request->cantidad,
     'valortotal'=>$request->valortotal
            // 'creado_por'=>Auth::id()
        ]);
       if($pedidoupdate){
   
          return response()->json([
                'ok'    => true,
                'menssage'  => 'pedido modificado exitosamente',
               
                ]);
} 


    }

    /**
     
    
     * @throws \Exception
     */
    public function destroy(Request $request)
    {$id=$request->id;
        $pedido = Pedido::find($id)->delete();
  if($pedido){

   return response()->json([
                'ok'    => true,
                'menssage'  => 'pedido eliminado exitosamente',
               
                ]);
}
        
    }
}
