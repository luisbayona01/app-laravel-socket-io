<?php

namespace App\Http\Controllers;

use App\Models\Cuenta;
use Illuminate\Http\Request;

/**
 * Class CuentumController
 * @package App\Http\Controllers
 */
class CuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     /* @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuenta = Cuenta::all();
       return $cuenta;
         }

 
    public function store(Request $request)
    {
        request()->validate(Cuenta::$rules);

        $cuenta = Cuenta::create($request->all());

 if ($cuenta){

  return response()->json([
                'ok'    => true,
                'menssage'  => 'operacion exitosa'
                ]);

}
        /*return redirect()->route('cuenta.index')
            ->with('success', 'Cuentum created successfully.');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
       */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     
     */
    public function edit(Request $request)
    { $id=$request->id;
        $cuenta = Cuenta::find($id);

        return view('cuenta.edit', compact('cuenta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
    
     */
    public function update(Request $request )
    {
        request()->validate(Cuenta::$rules);

      

$resupdate=Cuenta::where('idcuenta',$request->idcuenta)->update([
            		'nombre' => $request->nombre,
     'email'=> $request->email,
     'telefono'=> $request->telefono,
            // 'creado_por'=>Auth::id()
        ]);

if($resupdate){

return response()->json([
                'ok'    => true,
                'menssage'  => 'operacion exitosa'
                ]);

}
  

    }

    /**
     * @param int $id
    
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $id=$request->id;

        if(Cuenta::find($id)->delete()){

return response()->json([
                'ok'    => true,
                'menssage'  => 'operacion exitosa'
                ]);

}
  
    }
}
