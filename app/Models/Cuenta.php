<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cuentum
 *
 * @property $idcuenta
 * @property $nombre
 * @property $email
 * @property $telefono
 *
 * @property Pedido[] $pedidos
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Cuenta extends Model
{

  public $timestamps = false;
     protected $table = 'cuenta';  // tabla
     protected $primaryKey = 'idcuenta';
    static $rules = [
		'nombre' => 'required',
     'email'=> 'required',
     'telefono'=> 'required',
    ];

  

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['idcuenta','nombre','email','telefono'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'idcuenta', 'idcuenta');
    }
    

}
