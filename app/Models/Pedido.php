<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pedido
 *
 * @property $idpedido
 * @property $idcuenta
 * @property $producto
 * @property $cantidad
 * @property $valortotal
 *
 * @property Cuenta $cuenta
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Pedido extends Model
{   public $timestamps = false;
     protected $table = 'pedido';  // tabla

     protected $primaryKey = 'idpedido';
    static $rules = [
		'idcuenta' => 'required',
    ];

  

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['idpedido','idcuenta','producto','cantidad','valortotal'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cuenta()
    {
        return $this->hasOne('App\Models\Cuenta', 'idcuenta', 'idcuenta');
    }
    

}
