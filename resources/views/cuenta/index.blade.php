@extends('layouts.app')

@section('template_title')
    Cuenta
@endsection


@section('content')
<script>

$(document).ready(function(){
cuentas();


$("#savechanges").click(function(){
  var isValid = document.querySelector('#cuentas').reportValidity();
 if (isValid==false) {
    $('#cuentas').addClass('was-validated')
   //return false;
   }else{
let  url='{{ url('/createcuentas') }}';
let data=$("#cuentas").serialize();
    ajax(url, data);
    $("#cuentas")[0].reset();
  
    $("#cuentas").removeClass('was-validated')
 
   }
})

$("#updatecuenta").click(function(){
var isValid = document.querySelector('#formupdatecuenta').reportValidity();
 if (isValid==false) {
    $('#formupdatecuenta').addClass('was-validated')
   //return false;
   }else{
let  url='{{ url('/updatecuentas') }}';
let data=$("#formupdatecuenta").serialize();
    ajax(url, data);
  $("#formupdatecuenta").removeClass('was-validated')
 

   }

})


});


function  deleteRow(id){
 if (confirm("¿Seguro que desea Eliminar este Registro?\n       Esta acción NO se puede deshacer.")) {
   let data={id: id, "_token": "{{ csrf_token() }}"};
    let url = "{{url('/deletecuentas')}}";
  
 ajax(url,data);
  
  }
}


function ajax(url,data) {
 
$.ajax({
    data: data,
    type: "POST",
    dataType: "json",
    url: url,
})
 .done(function( dto, textStatus, jqXHR ) {
 alert(dto['menssage']);
  cuentas();
 })
 .fail(function( jqXHR, textStatus, errorThrown ) {
     if ( console && console.log ) {
         console.log( "La solicitud a fallado: " +  textStatus);
     }
});

}

function cuentas(){
$url = '{{url('/liscuentas')}}';

$.getJSON( $url, function( data ) {
  //var items = [];
  //console.log(data);
$(".cuerpocuenta").html("");
$.each( data, function( key, val ) {

 var tableRow = "<tr>"
                + "<td>" + val.idcuenta + "</td>"
                + "<td>" + val.nombre + "</td>"
                + "<td >" +val.email + "</td>"
                + "<td>" + val.telefono + "</td>"
                +"<td> <button ' class='btn btn-info btn-xs btn-editcustomer' onclick='showEditRow(" + val.idcuenta + ")'><i class='fa fa-pencil' aria-hidentificacionden='true'></i>Edit</button>" +
                "<button ' class='btn btn-danger btn-xs btn-deleteCustomer' onclick='deleteRow(" + val.idcuenta + ")'><i class='fa fa-trash' aria-hidentificacionden='true'>Delete</button>"
                + "</td>"
                + "</tr>";

 $(".cuerpocuenta").append(tableRow);
  });

 
  
});
}

function showEditRow(id){
console.log(id);
let parametros={id: id, "_token": "{{ csrf_token() }}"};
let url="{{url('/editcuentas/')}}";
$.ajax({
  method: "POST",
  url: url,
  data: parametros,
  
}).done(function (datos) {
  
$("#editcuentas").html(datos);
$("#editmodal").modal('show');
});

}


 


</script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                           <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#savemodal">
  Agregrar cuenta
</button>
                        </div>
                    </div>
               

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                       
                                        
										<th>Idcuenta</th>
										<th>Nombre</th>
										<th>Email</th>
										<th>Telefono</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="cuerpocuenta">
                                   
                                  
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
             
            </div>
        </div>
    </div>



<div class="modal fade" id="savemodal" tabindex="-1" aria-labelledby="savemodal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Cuenta</span>
                    </div>
                    <div class="card-body">
                        <form method="POST"   id="cuentas"enctype="multipart/form-data">
                            @csrf

                            <div class="box box-info padding-1">
    <div class="box-body">
        
       
        <div class="form-group">
            <label>nombre </label>
    <input type="text" class="form-control"  name="nombre" value="" placeholder="nombre" required>            
    </div>
        <div class="form-group">
            <label>email </label> 
            <input type="email" class="form-control"   name="email" value="" placeholder="email" multiple >
        </div>
        <div class="form-group">
               <label>telefono </label> 
             <input type="text" class="form-control"   name="telefono" value="" placeholder="telefono" required>
        </div>

    </div>

</div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="savechanges" >Save changes</button>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="editmodal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
        
         <div class="modal-header">
     
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      
      <div class="modal-body">
<div id="editcuentas">
</div>        

      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button>
        <button type="button" class="btn btn-primary" id="updatecuenta">Save changes</button>
      </div>
    </div>
  </div>
</div>

  
@endsection
