

    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

             

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Pedido</span>
                    </div>
                    <div class="card-body">
                        <form method="POST"  role="form" id="pedidoscreate">
                            @csrf
                            @include('pedido.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

