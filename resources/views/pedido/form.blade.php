<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group" style="display:none">
            {{ Form::label('idpedido') }}
            {{ Form::text('idpedido', $pedido->idpedido, ['class' => 'form-control' . ($errors->has('idpedido') ? ' is-invalid' : ''), 'placeholder' => 'Idpedido']) }}
            {!! $errors->first('idpedido', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('cuenta') }}
            {{ Form::select('idcuenta', $cuenta ,$pedido->idcuenta, ['class' => 'form-control' . ($errors->has('idcuenta') ? ' is-invalid' : ''), 'placeholder' => 'seleccione una cuenta']) }}
            {!! $errors->first('idcuenta', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('producto') }}
            {{ Form::text('producto', $pedido->producto, ['class' => 'form-control' . ($errors->has('producto') ? ' is-invalid' : ''), 'placeholder' => 'Producto']) }}
            {!! $errors->first('producto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('cantidad') }}
            {{ Form::text('cantidad', $pedido->cantidad, ['class' => 'form-control' . ($errors->has('cantidad') ? ' is-invalid' : ''), 'placeholder' => 'Cantidad']) }}
            {!! $errors->first('cantidad', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('valortotal') }}
            {{ Form::text('valortotal', $pedido->valortotal, ['class' => 'form-control' . ($errors->has('valortotal') ? ' is-invalid' : ''), 'placeholder' => 'Valortotal']) }}
            {!! $errors->first('valortotal', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    
</div>