
@extends('layouts.app')

@section('template_title')
    Pedido
@endsection


@section('content')
<script src="https://cdn.socket.io/4.0.1/socket.io.min.js" integrity="sha384-LzhRnpGmQP+lOvWruF/lgkcqD+WDVt9fU3H4BWmwP5u5LTmkUGafMcpZKNObVMLU" crossorigin="anonymous"></script>

<script>

$(document).ready(function(){

                let ip_address = '127.0.0.1';
                let socket_port = '3000';
                let socket = io(ip_address + ':' + socket_port);
pedidos();



function ajax(url,data) {
 
$.ajax({
    data: data,
    type: "POST",
    dataType: "json",
    url: url,
})
 .done(function( dto, textStatus, jqXHR ) {
 alert(dto['menssage']);
//console.log(dto['data'][0]);  
 if (dto['data']){
    //console.log('okdata',dto['data']);
  let data= dto['data'][0];

 //console.log(dto['data'][0]);
    socket.emit('sendPedidoToServer', data);

      
}

  pedidos();
 })
 .fail(function( jqXHR, textStatus, errorThrown ) {
     if ( console && console.log ) {
         console.log( "La solicitud a fallado: " +  textStatus);
     }
});

}


$("#formpedido").click(function(){
//$("#Savemodalpedido").modal()
//let parametros={"_token": "{{ csrf_token() }}"};
let url="{{url('/createpedidos/')}}";
$.ajax({
  method: "GET",
  url: url,
  data: '',
  
}).done(function (datos) {
  
$("#modalpedidoadd").html(datos);
$("#Savemodalpedido").modal('show');
});

})

$("#SavePedido").click(function(){
  var isValid = document.querySelector('#pedidoscreate').reportValidity();
 if (isValid==false) {
    $('#pedidoscreate').addClass('was-validated')
   //return false;
   }else{
let  url='{{ url('/storepedidos') }}';
let data=$("#pedidoscreate").serialize();

    ajax(url, data);
    $("#pedidoscreate")[0].reset();
  
    $("#pedidoscreate").removeClass('was-validated')
 
   }
})

$("#SavePedidoupdate").click(function(){
var isValid = document.querySelector('#updatepedido').reportValidity();
 if (isValid==false) {
    $('#updatepedido').addClass('was-validated')
   //return false;
   }else{
let  url='{{ url('/updatepedidos') }}';
let data=$("#updatepedido").serialize();
    ajax(url, data);
  $("#updatepedido").removeClass('was-validated')
 

   }

})

 socket.on('sendPedidoToClient', (data) => {
 var tableRow = "<tr>"
                + "<td>" + data.idpedido + "</td>"
                + "<td>" + data.producto + "</td>"
                + "<td>" + data.cuenta + "</td>"
                + "<td >" +data.cantidad + "</td>"
                + "<td>" + data.valortotal + "</td>"
                +"<td> <button ' class='btn btn-info btn-xs btn-editcustomer' onclick='showEditRow(" + data.idpedido + ")'><i class='fa fa-pencil' aria-hidentificacionden='true'></i>Edit</button>" +
                "<button ' class='btn btn-danger btn-xs btn-deleteCustomer' onclick='deleteRow(" + data.idpedido + ")'><i class='fa fa-trash' aria-hidentificacionden='true'>Delete</button>"
                + "</td>"
                + "</tr>";

 $(".cuerpopedido").append(tableRow);
  

                   
                });
});






function pedidos(){
$url = '{{url('/listpedidos')}}';

$.getJSON( $url, function( data ) {
  //var items = [];
  //console.log(data);
$(".cuerpopedido").html("");
$.each( data, function( key, val ) {

 var tableRow = "<tr>"
                + "<td>" + val.idpedido + "</td>"
                + "<td>" + val.producto + "</td>"
                + "<td>" + val.cuenta + "</td>"
                + "<td >" +val.cantidad + "</td>"
                + "<td>" + val.valortotal + "</td>"
                +"<td> <button ' class='btn btn-info btn-xs btn-editcustomer' onclick='showEditRow(" + val.idpedido + ")'><i class='fa fa-pencil' aria-hidentificacionden='true'></i>Edit</button>" +
                "<button ' class='btn btn-danger btn-xs btn-deleteCustomer' onclick='deleteRow(" + val.idpedido + ")'><i class='fa fa-trash' aria-hidentificacionden='true'>Delete</button>"
                + "</td>"
                + "</tr>";

 $(".cuerpopedido").append(tableRow);
  });

 
  
});
}

function  deleteRow(id){
 if (confirm("¿Seguro que desea Eliminar este Registro?\n       Esta acción NO se puede deshacer.")) {
   let data={id: id, "_token": "{{ csrf_token() }}"};
    let url = "{{url('/deletepedidos')}}";

$.post(url,data)
  .done(function( datos ) {
    alert(datos['menssage'] );
pedidos();
  });
  
  }
}

function showEditRow(id){
console.log(id);
let parametros={id: id, "_token": "{{ csrf_token() }}"};
let url="{{url('/editpedidos/')}}";
$.ajax({
  method: "POST",
  url: url,
  data: parametros,
  
}).done(function (datos) {
 
$("#editpedidos").html(datos);
var myModaledit = new bootstrap.Modal(document.getElementById("editp"), {});
myModaledit.show();
});

}


 


</script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                              
                            </span>

                             <div class="float-right">
                        <button  type="buuton" class="btn btn-info" id="formpedido">Add pedido </button>
                              </div>
                        </div>
                    </div>
                     
                                

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="table">
                                <thead class="thead">
                                    <tr>
                                   
                                        
										<th>Idpedido</th>
										<th>cuenta</th>
										<th>Producto</th>
										<th>Cantidad</th>
										<th>Valortotal</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="cuerpopedido">
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>



<div class="modal fade" id="Savemodalpedido" tabindex="-1" role="dialog" aria-labelledby="Savemodalpedido" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
        
         <div class="modal-header">
     
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      
      <div class="modal-body">
<div id="modalpedidoadd">
</div>        

      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button>
        <button type="button" class="btn btn-primary" id="SavePedido">Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="editp" tabindex="-1" role="dialog" aria-labelledby="editp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
        
         <div class="modal-header">
     
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      
      <div class="modal-body">
<div id="editpedidos">
</div>        

      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button>
        <button type="button" class="btn btn-primary" id="SavePedidoupdate">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection
